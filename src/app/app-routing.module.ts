import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'laps', loadChildren: './tab1/laps/laps.module#LapsPageModule' },
  { path: 'graphs', loadChildren: './tab1/graphs/graphs.module#GraphsPageModule' },
  { path: 'graph-modal', loadChildren: './tab1/graphs/graph-modal/graph-modal.module#GraphModalPageModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

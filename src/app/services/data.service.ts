import { Injectable } from '@angular/core';

import { Http } from '@angular/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private URL = environment.backend;

  constructor(private http: Http) { }

  getFilesFromServer(): Observable<any[]> {
    return this.http.get(`${this.URL}/files`)
      .pipe(
        map(response => response.json()),
        catchError(this.handleError)
        // catchError(val => of(`I caught: ${val}`))
      );
  }

  getSessionFromServer(name: string): Observable<any> {
    return this.http.get(`${this.URL}/sessions/${name}`)
      .pipe(
        map(response => response.json()),
        catchError(this.handleError)
      );
  }

  getLapsFromServer(): Observable<any> {
    return this.http.get(`${this.URL}/laps`)
      .pipe(
        map(response => response.json()),
        catchError(this.handleError)
      );
  }

  getLapFromServer(name: string, lap: any): Observable<any> {
    return this.http.get(`${this.URL}/laps/${name}/${lap}`)
      .pipe(
        map(response => response.json()),
        catchError(this.handleError)
      );
  }

  getLapDataPerParamFromServer(name: string, lap: any, param: string): Observable<any> {
    return this.http.get(`${this.URL}/params/${name}/${lap}/${param}`)
      .pipe(
        map(response => response.json()),
        catchError(this.handleError)
      );
  }

  private handleError(error: Response | any) {
    // console.log(error.message || error);
    return throwError(error.message || error);
  }
}

export class SessionData {
    fileName: string;
    venue: string;
    vehicleId: string;
    user: string;
    date: string;
    time: string;
    duration: string;
    lapCount: number;
    headers: any[];
    data: any[];

    constructor(
        fileName: string,
        venue: string,
        vehicleId: string,
        user: string,
        date: string,
        time: string,
        duration: string,
        lapCount: number,
        headers: any[],
        data: any[]) {
        this.fileName = fileName;
        this.venue = venue;
        this.vehicleId = vehicleId;
        this.user = user;
        this.date = date;
        this.time = time;
        this.duration = duration;
        this.lapCount = lapCount;
        this.headers = headers;
        this.data = data;
    }
}

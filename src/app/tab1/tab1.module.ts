import { IonicModule } from '@ionic/angular';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { PopoverComponent } from './popover/popover.component';
import { SessionCardComponent } from './session-card/session-card.component';

const routes: Routes = [
  {
    path: '',
    component: Tab1Page,
    children: [
      {
        path: 'laps',
        children: [
          {
            path: '',
            loadChildren: './laps/laps.module#LapsPageModule'
          },
          {
            path: '',
            redirectTo: '/laps',
            pathMatch: 'full'
          }
        ]
      }
    ]
  }
];
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    Tab1Page,
    PopoverComponent,
    SessionCardComponent
  ],
  entryComponents: [
    PopoverComponent
  ],
})
export class Tab1PageModule { }

import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { DataService } from '../../services/data.service';
import { State } from 'src/app/models/state';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss']
})
export class PopoverComponent implements OnInit {
  state = null;
  error = null;
  color = null;

  constructor(private navParams: NavParams, private dataService: DataService) { }

  ngOnInit() {
    this.error = this.navParams.get('session').error;

    const state = this.navParams.get('session').state;
    switch (state) {
      case State.Failed:
        this.state = 'Not parsed';
        this.color = 'danger';
        break;
      case State.Initial:
        this.state = state;
        this.color = 'secondary';
        break;
      case State.Loaded:
        this.state = 'Parsed on Server';
        this.color = 'success';
        break;
      case State.Loading:
        this.state = state;
        this.color = 'primary';
        break;
      default:
        this.state = state;
        this.color = 'secondary';
    }
  }

  download() {
    const name = this.navParams.get('session').name;
    window.open(`${environment.backend}/files/download/${name}`, '_blank');
  }
}

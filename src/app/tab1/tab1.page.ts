import { Component } from '@angular/core';
import { DataService } from '../services/data.service';
import { LoadingController, ToastController, PopoverController } from '@ionic/angular';
import { DisplaySession } from './display-session';
import { PopoverComponent } from './popover/popover.component';
import { State } from '../models/state';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  sessionsParsed = false;
  sessions: DisplaySession[] = [];
  sessionsSelected: any[] = [];

  constructor(private dataService: DataService,
    public loadingCtrl: LoadingController, public toastController: ToastController, public popoverController: PopoverController) {
    this.init();
  }

  async init() {
    const loading = await this.loadingCtrl.create({ message: 'Loading Sessions' });
    loading.present().then(() => {
      this.dataService.getFilesFromServer().subscribe(
        data => {

          /** Sort array by modified timestamp */
          data.sort(function (a, b) {
            return a.mtime - b.mtime;
          });

          /** Display list  */
          this.sessionsParsed = true;
          for (const entry of data) {
            if (!entry.parsed) {
              this.sessionsParsed = false;
            }

            const displayName = entry.name + entry.extension;
            const state = entry.parsed ? State.Loaded : State.Failed;
            this.sessions.push(new DisplaySession(displayName, state));
          }
          loading.dismiss();
        },
        error => {
          this.presentToast('Error while loading sessions.');
          loading.dismiss();
        }
      );
    });
  }

  // async loadSessions() {
  //   for (let i = 0; i < this.sessions.length; i++) {
  //     this.sessions[i].state = State.Loading;
  //     console.log(this.sessions[i]);
  //     await this.dataService.getSessionFromServer(this.sessions[i].name).toPromise()
  //       .then(res => {
  //         console.log(res);
  //         this.sessions[i].state = State.Loaded;
  //         return;
  //       })
  //       .catch((error) => {
  //         this.sessions[i].state = State.Failed;
  //         this.sessions[i].error = error;
  //         return;
  //       });
  //   }
  //   this.sessionsParsed = true;
  // }

  async presentPopover(ev: any, session: DisplaySession) {
    const popover = await this.popoverController.create({
      componentProps: { session: session },
      component: PopoverComponent,
      event: ev,
      translucent: false,
      showBackdrop: true,
      animated: true
    });
    return await popover.present();
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      position: 'top',
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async updateMetadata(sessionName) {
    const fileName = sessionName.includes('.csv') ? sessionName : sessionName + '.csv'; // FIX
    sessionName = sessionName.includes('.csv') ? sessionName.split('.').slice(0, -1).join('.') : sessionName; // FIX

    /** Remove displayed session */
    for (let i = 0; i < this.sessionsSelected.length; i++) {
      if (this.sessionsSelected[i].sessionName === sessionName) {
        this.sessionsSelected.splice(i, 1);
        return;
      }
    }

    /** Show indicator for loading session data */
    let j: number;
    for (j = 0; j < this.sessions.length; j++) {
      if (this.sessions[j].name === fileName) {
        this.sessions[j].state = State.Loading;
        break;
      }
    }

    const sessionData = await this.dataService.getSessionFromServer(sessionName).toPromise();
    this.sessions[j].state = State.Loaded;
    // if (this.sessionsSelected.length > 2) {
    //   this.removeCard(this.sessionsSelected[0].sessionName);
    // }
    this.sessionsSelected.push(sessionData);
  }

  removeCard(sessionName) {
    const fileName = sessionName + '.csv'; // FIX

    for (let i = 0; i < this.sessions.length; i++) {
      if (this.sessions[i].name === fileName) {
        this.sessions[i].isChecked = false;
        this.updateMetadata(sessionName);
        break;
      }
    }
  }
}

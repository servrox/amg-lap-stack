import { State } from '../models/state';

export class DisplaySession {
    name: string;
    state: State;
    error: any;
    isChecked: boolean;

    constructor(
        name: string,
        state: State = State.Initial,
        error: any = false,
        isChecked: boolean = false) {
        this.name = name;
        this.state = state;
        this.error = error;
        this.isChecked = isChecked;
    }
}

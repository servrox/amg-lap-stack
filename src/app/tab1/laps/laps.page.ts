import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { NavController, LoadingController } from '@ionic/angular';
import { LapRow } from './lap-row.model';

@Component({
  selector: 'app-laps',
  templateUrl: './laps.page.html',
  styleUrls: ['./laps.page.scss'],
})
export class LapsPage implements OnInit {
  routeParams: string;
  displayTable = false;
  selected = [];
  rows: LapRow[] = [];
  columns = [
    { prop: 'Full Lap' },
    { prop: 'Duration' },
    { prop: 'Session' },
    { prop: 'Lap No.' }
  ];

  constructor(private dataService: DataService, public navCtrl: NavController, public loadingController: LoadingController) { }

  ngOnInit() {
    this.init();
  }

  async init() {
    const loading = await this.loadingController.create();
    loading.present();

    this.dataService.getLapsFromServer().subscribe((laps) => {
      if (laps.length === 0) {
        this.navCtrl.navigateBack('/');
      } else {
        this.fillTable(laps);
        this.displayTable = true;
        loading.dismiss();
      }
    });
  }

  fillTable(allLapData) {
    for (let i = 0; i < allLapData.length; i++) {
      this.rows.push(new LapRow(allLapData[i].fullLap, allLapData[i].durationFormatted, allLapData[i].sessionName, allLapData[i].lapNo));
    }
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    this.setRouteParams();
  }

  private setRouteParams() {
    let params = '';
    for (let i = 0; i < this.selected.length; i++) {
      // tslint:disable-next-line:max-line-length
      const str = `${params !== '' ? ',' : ''}${this.selected[i].Session};${this.selected[i]['Lap No.']};${this.selected[i].Duration};${this.selected[i]['Full Lap'] === '🏁' ? 'true' : 'false'}`;
      params = params === '' ? str : params.concat(str);
    }
    this.routeParams = params;
  }
}

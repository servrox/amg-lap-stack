import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { IonicModule } from '@ionic/angular';

import { LapsPage } from './laps.page';

const routes: Routes = [
  {
    path: '',
    component: LapsPage,
    children: [
      {
        path: 'graphs',
        children: [
          {
            path: '',
            loadChildren: '../graphs/graphs.module#GraphsPageModule'
          },
          {
            path: '',
            redirectTo: '/graphs',
            pathMatch: 'full'
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    NgxDatatableModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LapsPage]
})
export class LapsPageModule { }

export class LapRow {
    'Full Lap': string;
    'Duration': string;
    'Session': string;
    'Lap No.': number;

    constructor(
        fullLap: boolean,
        duration: string,
        sessionName: string,
        lapNo: number) {
        this['Full Lap'] = fullLap ? '🏁' : ' ';
        this.Duration = duration;
        this.Session = sessionName;
        this['Lap No.'] = lapNo;
    }
}

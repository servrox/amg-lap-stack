import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, LoadingController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import regression from 'regression';
@Component({
  selector: 'app-graph-modal',
  templateUrl: './graph-modal.page.html',
  styleUrls: ['./graph-modal.page.scss'],
})
export class GraphModalPage implements OnInit {
  paramName: string;
  name: string;
  laps: any[];

  chartLoaded = false;
  lineChartData: Array<any> = [];
  lineChartLabels: Array<any> = [];
  lineChartOptions: any = {
    responsive: true
  };
  lineChartLegend = true;
  lineChartType = 'line';

  constructor(private dataService: DataService,
    public modalController: ModalController, private navParams: NavParams, public loadingController: LoadingController) { }

  ngOnInit() {
    this.paramName = this.navParams.get('paramName');
    this.laps = this.navParams.get('laps');

    this.loadData();
  }

  async loadData() {
    const loading = await this.loadingController.create();
    loading.present();

    const countDatasets = [];
    let name: string;
    let lapNo: number;
    for (let i = 0; i < this.laps.length; i++) {
      name = this.laps[i].name;
      lapNo = this.laps[i].lap;

      await this.dataService.getLapDataPerParamFromServer(name, lapNo, this.paramName).toPromise()
        .then(res => {
          countDatasets.push(res.length);
          this.lineChartData.push(this.decorateLine(res, name, lapNo));
          return;
        })
        .catch((error) => {
          console.log(error);
          return;
        });
    }

    // const reg = this.getRegressionLine(this.lineChartData[0].data);
    // const reg2 = this.getRegressionLine(this.lineChartData[1].data);
    // this.lineChartData.push(this.decorateLine(reg, name, lapNo));
    // this.lineChartData.push(this.decorateLine(reg2, name, lapNo));

    const mostDatasets = Math.max.apply(Math, countDatasets);
    let count = 0.000;
    for (let i = 0; i < mostDatasets; i++) {
      count = count + 0.020;
      this.lineChartLabels.push('' + count + '');
    }

    this.chartLoaded = true;
    loading.dismiss();
  }

  private decorateLine(res: any[], name: string, lap: number): any {
    return { data: res, label: `${name} - ${lap}`, options: { fill: false, pointRadius: 0 } };
  }

  private getRegressionLine(data) {
    const reg = this.getRegression(data, 4);
    const insert = [];
    reg.forEach(function (item) {
      insert.push(item);
    });
    return insert;
  }

  private getRegression(data, degre) {
    degre = degre || 2;
    const dataRegression = [];
    data.forEach((element, index) => dataRegression.push([index + 1, element]));

    const resultRegression = [];
    // regression('polynomial', dataRegression, degre).points.forEach((element) =>
    regression.polynomial(dataRegression, {
      order: 5,
      precision: 5,
    }).points.forEach((element) =>
      resultRegression.push(Math.ceil(element[1] * 100) / 100)
    );
    return resultRegression;
  }

  close() {
    this.modalController.dismiss();
  }
}

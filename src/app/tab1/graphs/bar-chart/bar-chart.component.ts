import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { GraphModalPage } from '../graph-modal/graph-modal.page';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {
  @Input() barChartData: any[];
  @Input() barChartLabels: string[];

  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  barChartType = 'bar';
  barChartLegend = true;
  highlight: string;

  constructor(public modalController: ModalController) { }

  ngOnInit() {
    const keyParams = ['InlineAcc', 'LateralAcc', 'VerticalAcc', 'YawRate', 'RollRate', 'PitchRate', 'ECU_RPM',
      'ECU_THROTTLE', 'ECU_BRK_STATE', 'ECU_SPEED', 'ECU_GEAR', 'ECU_STER_ANG'];
    if (keyParams.includes(this.barChartLabels[0])) {
      this.highlight = '#f2f7ff';
    } else {
      this.highlight = 'initial';
    }
  }

  async presentModal() {
    const laps = [];
    for (let i = 0; i < this.barChartData.length; i++) {
      const lap = {
        name: this.barChartData[i].label.split(' - ')[0],
        lap: Number(this.barChartData[i].label.split(' - ')[1])
      };
      laps.push(lap);
    }

    const modal = await this.modalController.create({
      cssClass: 'modal-fullscreen',
      componentProps: {
        laps: laps,
        paramName: this.barChartLabels[0]
      },
      component: GraphModalPage
    });
    return await modal.present();
  }
}

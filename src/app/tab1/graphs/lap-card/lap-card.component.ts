import { Component, OnInit, Input } from '@angular/core';
import { State } from 'src/app/models/state';

@Component({
  selector: 'app-lap-card',
  templateUrl: './lap-card.component.html',
  styleUrls: ['./lap-card.component.scss']
})
export class LapCardComponent implements OnInit {

  @Input() state: State;
  @Input() fullLap: string;
  @Input() duration: string;
  @Input() session: string;
  @Input() lap: number;

  constructor() { }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LapCardComponent } from './lap-card.component';

describe('LapCardComponent', () => {
  let component: LapCardComponent;
  let fixture: ComponentFixture<LapCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LapCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LapCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

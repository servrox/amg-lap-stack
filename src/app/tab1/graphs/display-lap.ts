import { State } from 'src/app/models/state';

export class DisplayLap {
    fullLap: string;
    duration: string;
    session: string;
    lap: number;
    state: State;
    error: any;

    constructor(
        fullLap: boolean,
        duration: string,
        session: string,
        lap: number,
        state: State = State.Initial,
        error: any = false) {
        this.fullLap = fullLap ? '🏁' : ' ';
        this.duration = duration;
        this.session = session;
        this.lap = lap;
        this.state = state;
        this.error = error;
    }
}
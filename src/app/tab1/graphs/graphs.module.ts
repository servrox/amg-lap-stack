import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GraphsPage } from './graphs.page';
import { ChartsModule } from 'ng2-charts';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { LapCardComponent } from './lap-card/lap-card.component';
import { GraphModalPage } from './graph-modal/graph-modal.page';

const routes: Routes = [
  {
    path: '',
    component: GraphsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ChartsModule
  ],
  declarations: [
    GraphsPage,
    BarChartComponent,
    LapCardComponent,
    GraphModalPage
  ],
  entryComponents: [
    GraphModalPage
  ],
})
export class GraphsPageModule { }

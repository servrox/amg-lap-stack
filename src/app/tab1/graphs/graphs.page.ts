import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { NavController } from '@ionic/angular';
import { DisplayLap } from './display-lap';
import { State } from 'src/app/models/state';

@Component({
  selector: 'app-graphs',
  templateUrl: './graphs.page.html',
  styleUrls: ['./graphs.page.scss'],
})
export class GraphsPage implements OnInit {
  data = [];
  displayLaps: DisplayLap[] = [];

  barCharts = [];
  barChartsLoaded = false;

  constructor(private dataService: DataService, private route: ActivatedRoute, public navCtrl: NavController) { }

  ngOnInit() {
    const params = this.route.snapshot.paramMap.get('go').split(',').map((arr) => arr.split(';'));
    for (let i = 0; i < params.length; i++) {
      const fullLap = (params[i][3] === 'true');
      const duration = params[i][2];
      const session = params[i][0];
      const lap = Number(params[i][1]);
      this.displayLaps.push(new DisplayLap(fullLap, duration, session, lap));
    }
    this.loadAvgLaps();
  }

  async loadAvgLaps() {
    for (let i = 0; i < this.displayLaps.length; i++) {
      this.displayLaps[i].state = State.Loading;
      await this.dataService.getLapFromServer(this.displayLaps[i].session, this.displayLaps[i].lap).toPromise()
        .then(res => {
          this.displayLaps[i].state = State.Loaded;
          this.data.push(res);
          return;
        })
        .catch((error) => {
          this.displayLaps[i].state = State.Failed;
          this.displayLaps[i].error = error;
          return;
        });
    }
    this.decorateBarChart();
  }

  decorateBarChart() {
    let bar: {};
    const data = this.data;
    const params = Object.keys(data[0].averages[0]);

    let index = params.indexOf('Time');
    if (index > -1) {
      params.splice(index, 1);
    }
    index = params.indexOf('Distance');
    if (index > -1) {
      params.splice(index, 1);
    }

    for (let i = 0; i < params.length; i++) {
      const selectedParam = params[i];
      const bars = [];

      for (let j = 0; j < data.length; j++) {
        const valueParam = data[j].averages[0][selectedParam];
        const labelForBar = `${data[j].sessionName} - ${data[j].lapNo}`;

        bar = { data: [valueParam], label: labelForBar };
        bars.push(bar);
      }

      this.barCharts.push({ barChartLabels: [selectedParam], barChartData: bars });
    }

    this.barChartsLoaded = true;
  }
}

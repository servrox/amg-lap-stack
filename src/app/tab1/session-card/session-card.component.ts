import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-session-card',
  templateUrl: './session-card.component.html',
  styleUrls: ['./session-card.component.scss']
})
export class SessionCardComponent implements OnInit {

  @Input() sessionName: string;
  @Input() venue: string;
  @Input() vehicleId: string;
  @Input() user: string;
  @Input() date: string;
  @Input() time: string;
  @Input() fullLaps: string;
  @Input() duration: string;
  @Input() datasetCount: number;
  @Output() closeMe = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  close(sessionName: string) {
    this.closeMe.emit(sessionName);
  }
}

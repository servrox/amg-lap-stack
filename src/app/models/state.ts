export enum State {
    Initial = 'Initial',
    Loading = 'Loading',
    Loaded = 'Loaded',
    Failed = 'Failed'
}
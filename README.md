# AMG Lap

App for visualising your race sessions.

``` 
http://amg.servrox.solutions/
```

With this app you can compare your lap data from your race sessions.
All data is parsed and served by a backend server. Sessions must first be uploaded as a .csv file.
For data supply and storage is a mongodb.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Pre-requisites
* git - [Installation guide](https://www.linode.com/docs/development/version-control/how-to-install-git-on-linux-mac-and-windows/) .  
* node.js - [Download page](https://nodejs.org/en/download/) .  
* npm - comes with node or download yarn - [Download page](https://yarnpkg.com/lang/en/docs/install) .  
* mongodb - [Download page](https://www.mongodb.com/download-center/community) 

### Installation 
``` 
git clone git@gitlab.com:servrox1337/amg-lap-stack.git
cd amg-lap-stack
npm i
> npm run firststart (to build the frontend and start the server)
> npm start (to start the server, to serve the UI www/ folder should be existend)
> npm run dev (for server + ui development, starts a live reloading server and a second one for app dev/testing only)
> npm run devserver (for server development, starts a live server and builds the ui)
> npm run devui (for server development, starts the backend server and a live reloading server for the the ui)
```

### Docker based 
``` 
git clone git@gitlab.com:servrox1337/amg-lap-stack.git
cd amg-lap-stack
docker-compose up -d
```

## Enviroment variables

There's a .env file which sets enviroment variables. 
When deplyoing to a cloud provider remember that there are propably given enviroment variables you have to use.

```
NODE_ENV=<development || production>
SERVER_PORT=<PORT>
MONGO_HOST=<mongodb://mongo/odmp>
```

## Built With

* [Ionic 4](https://beta.ionicframework.com/docs/) - The web framework used
* [Express](https://expressjs.com/de/4x/api.html) - Core of the backend and API 
* [Mongoose](https://mongoosejs.com/) - For mongodb object modeling 
* [ng2-charts](https://valor-software.com/ng2-charts/) - As a fast way to display fancy graphs  

## Authors

* **Marcel Mayer**

const express = require('express');

const sessionRoutes = require('./sessions.route');
const lapsRoutes = require('./laps.route');
const paramsRoutes = require('./params.route');
const filesRoutes = require('./files.route');

const router = express.Router();

router.get('/health-check', (req, res) =>
  res.send('OK')
);

router.use('/sessions', sessionRoutes);
router.use('/laps', lapsRoutes);
router.use('/params', paramsRoutes);
router.use('/files', filesRoutes);

module.exports = router;
const express = require('express');
const asyncHandler = require('express-async-handler');

const fileCtrl = require('../controllers/file.controller');

const router = express.Router();
module.exports = router;

router.get('/', asyncHandler(getAllFiles));
router.get('/download/:file', asyncHandler(downloadFile));

async function getAllFiles(req, res) {
    const params = await fileCtrl.getAllFilesFromDb();
    res.json(params);
}

async function downloadFile(req, res) {
    const file = req.params.file;
    res.download('./server/sessions/' + file);
}
const express = require('express');
const asyncHandler = require('express-async-handler');

const sessionsCtrl = require('../controllers/sessions.controller');

const router = express.Router();
module.exports = router;

router.get('/', asyncHandler(getAllSessions));
router.get('/:name', asyncHandler(getSession));

async function getAllSessions(req, res) {
    const sessions = await sessionsCtrl.getAllSessions();
    res.json(sessions);
}

async function getSession(req, res) {
    const name = req.params.name;
    const session = await sessionsCtrl.getSessionByFileName(name);
    res.json(session);
}
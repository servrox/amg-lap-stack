const express = require('express');
const asyncHandler = require('express-async-handler');
const paramsCtrl = require('../controllers/params.controller');

const router = express.Router();
module.exports = router;

router.get('/:name/:lap', asyncHandler(getAllParametersForLap));
router.get('/:name/:lap/:param', asyncHandler(getParameterForLap));

async function getAllParametersForLap(req, res) {
    const name = req.params.name;
    const lapNo = req.params.lap;
    const params = await paramsCtrl.getAllParametersForLap(name, lapNo);
    res.json(params);
}

async function getParameterForLap(req, res) {
    const name = req.params.name;
    const lapNo = req.params.lap;
    const param = req.params.param;
    const paramValues = await paramsCtrl.getParameterForLap(name, lapNo, param);
    res.json(paramValues);
}
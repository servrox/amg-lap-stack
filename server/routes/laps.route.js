const express = require('express');
const asyncHandler = require('express-async-handler');
const lapsCtrl = require('../controllers/laps.controller');

const router = express.Router();
module.exports = router;

router.get('/', asyncHandler(getAllLaps));
router.get('/:name/:lap', asyncHandler(getLap));

async function getAllLaps(req, res) {
    const laps = await lapsCtrl.getAllLaps();
    res.json(laps);
}

async function getLap(req, res) {
    const name = req.params.name;
    const lapNo = req.params.lap;
    const lap = await lapsCtrl.getLapByFileNameAndNo(name, lapNo);
    res.json(lap);
}
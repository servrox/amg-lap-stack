const mongoose = require('mongoose');

const FileSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    extension: {
        type: String,
        required: true
    },
    sizeInByte: {
        type: Number,
        required: true
    },
    mtime: {
        type: Number,
        required: true
    },
    ctime: {
        type: Number,
        required: true
    },
    birthtime: {
        type: Number,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    parsed: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('File', FileSchema);
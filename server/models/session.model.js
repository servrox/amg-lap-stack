const mongoose = require('mongoose');

const SessionSchema = new mongoose.Schema({
    fileId: String,
    sessionName: String,
    venue: String,
    vehicleId: String,
    user: String,
    date: String,
    time: String,
    duration: String,
    datasetCount: Number,
    fullLaps: Number,
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Session', SessionSchema);
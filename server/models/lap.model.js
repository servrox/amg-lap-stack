const mongoose = require('mongoose');

const LapSchema = new mongoose.Schema({
    fileId: String,
    sessionId: String,
    sessionName: String,
    lapNo: Number,
    startDatasetFileNo: Number,
    endDatasetFileNo: Number,
    fullLap: Boolean,
    duration: String,
    durationFormatted: String,
    averages: Array,
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Lap', LapSchema);
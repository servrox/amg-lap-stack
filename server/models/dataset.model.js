const mongoose = require('mongoose');

const DatasetSchema = new mongoose.Schema({
    fileId: String,
    sessionId: String,
    lapId: String,
    datasetFileNo: Number,
    parameters: Object,
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Dataset', DatasetSchema);
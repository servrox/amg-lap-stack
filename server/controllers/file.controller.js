const fs = require('fs');
const path = require('path');

const File = require('../models/file.model');

const parserCtrl = require('./parser.controller');

async function checkForNew() {
  readFiles('./server/sessions/', (name, ext, sizeInByte, mtime, ctime, birthtime) => {
    File.find({
      name: name,
      extension: ext
    }).exec((err, docs) => {
      if (err) {
        console.log('Error finding File', err);
        return;
      } else if (docs.length === 0) {
        insert(name, ext, sizeInByte, mtime, ctime, birthtime);
      } else if (docs.length > 0) {
        for (let i = 1; i < docs.length; i++) {
          docs[i].remove()
            .then(res => {
              console.log('Removed File', res);
              return;
            })
            .catch((error) => {
              console.log('Error removing File', error);
              return;
            });
        }
        if (!docs[0].parsed) {
          parserCtrl.parseFile(docs[0].name, docs[0].extension);
        }
      }
    });
  });
}

function readFiles(dir, processFile) {
  // read directory
  fs.readdir(dir, (error, fileNames) => {
    if (error) throw error;

    fileNames.forEach(filename => {
      const name = path.parse(filename).name;
      const ext = path.parse(filename).ext;
      const filepath = path.resolve(dir, filename);

      fs.stat(filepath, function (error, stat) {
        if (error) throw error;

        if (stat.isFile()) {
          processFile(name, ext, stat.size, stat.mtime, stat.ctime, stat.birthtime);
        }
      });
    });
  });
}

async function insert(name, ext, sizeInByte, mtime, ctime, birthtime) {
  const file = {
    name: name,
    extension: ext,
    sizeInByte: sizeInByte,
    mtime: mtime,
    ctime: ctime,
    birthtime: birthtime
  };
  return new File(file).save()
    .then(res => {
      console.log('Saved File: ', res.name + res.extension);
      parserCtrl.parseFile(res.name, res.extension);
      return;
    })
    .catch((error) => {
      console.log('Error saving File', error);
      return;
    });
}

function getAllFilesFromDb() {
  return File.find()
    .exec()
    .then((docs) => {
      return docs;
    })
    .catch((err) => {
      console.log('Error finding Files.', err);
      return {};
    });
}


module.exports = {
  checkForNew,
  insert,
  getAllFilesFromDb
}
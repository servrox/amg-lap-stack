const Lap = require('../models/lap.model');
const File = require('../models/file.model');
const Dataset = require('../models/dataset.model');

async function getAllParametersForLap(name, lapNo) {
    const fileId = await File.findOne({
            name: name
        })
        .exec()
        .then((doc) => {
            return doc.id;
        })
        .catch((err) => {
            console.log('Error finding File', err);
            return;
        });

    const lapId = await Lap.findOne({
            fileId: fileId,
            lapNo: lapNo
        })
        .exec()
        .then((doc) => {
            return doc.id;
        })
        .catch((err) => {
            console.log('Error finding Session.', err);
            return {};
        });

    return datasets = await Dataset.find({
            fileId: fileId,
            lapId: lapId
        })
        .exec()
        .then((docs) => {
            return docs;
        })
        .catch((err) => {
            console.log('Error finding Session.', err);
            return {};
        });
}

async function getParameterForLap(name, lapNo, param) {
    const datasets = await getAllParametersForLap(name, lapNo);

    const paramValues = [];
    for (let i = 0; i < datasets.length; i++) {
        paramValues.push(datasets[i].parameters[param]);
    }

    return paramValues;
}

module.exports = {
    getAllParametersForLap,
    getParameterForLap
}
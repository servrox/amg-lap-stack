const Session = require('../models/session.model');
const File = require('../models/file.model');

function getAllSessions() {
    return Session.find()
        .exec()
        .then((docs) => {
            return docs;
        })
        .catch((err) => {
            console.log('Error finding Sessions.', err);
            return {};
        });
}

async function getSessionByFileName(name) {
    const fileId = await File.findOne({
            name: name
        })
        .exec()
        .then((doc) => {
            return doc.id;
        })
        .catch((err) => {
            console.log('Error finding File', err);
            return;
        });

    return Session.findOne({
            fileId: fileId
        })
        .exec()
        .then((doc) => {
            return doc;
        })
        .catch((err) => {
            console.log('Error finding Session.', err);
            return {};
        });
}

module.exports = {
    getAllSessions,
    getSessionByFileName
}
const Lap = require('../models/lap.model');
const File = require('../models/file.model');

function getAllLaps() {
    return Lap.find()
        .exec()
        .then((docs) => {
            return docs;
        })
        .catch((err) => {
            console.log('Error finding Sessions.', err);
            return {};
        });
}

async function getLapByFileNameAndNo(name, lapNo) {
    const fileId = await File.findOne({
            name: name
        })
        .exec()
        .then((doc) => {
            return doc.id;
        })
        .catch((err) => {
            console.log('Error finding File', err);
            return;
        });

    return Lap.findOne({
            fileId: fileId,
            lapNo: lapNo
        })
        .exec()
        .then((doc) => {
            return doc;
        })
        .catch((err) => {
            console.log('Error finding Session.', err);
            return {};
        });
}

module.exports = {
    getAllLaps,
    getLapByFileNameAndNo
}
const csv = require('fast-csv');

const utilCtrl = require('./util.controller');

const File = require('../models/file.model');
const Session = require('../models/session.model');
const Lap = require('../models/lap.model');
const Dataset = require('../models/dataset.model');

const parseQueue = [];
let parserRunning = false;

async function saveSession(fileId, sessionName, venue, vehicleId, user, date, time, duration, datasetCount, fullLaps) {
    const session = {
        fileId: fileId,
        sessionName: sessionName,
        venue: venue,
        vehicleId: vehicleId,
        user: user,
        date: date,
        time: time,
        duration: duration,
        datasetCount: datasetCount,
        fullLaps: fullLaps
    };
    return new Session(session).save()
        .then(res => {
            console.log('Saved parsed Session from File: ' + res.fileId + ' with ' + res.datasetCount + ' datasets.');
            return res.id;
        })
        .catch((error) => {
            console.log('Error saving parsed Session', error);
            return;
        });
}

async function saveLap(fileId, sessionId, sessionName, lapNo, startDatasetFileNo, endDatasetFileNo, fullLap, duration, durationFormatted, averages) {
    const lap = {
        fileId: fileId,
        sessionId: sessionId,
        sessionName: sessionName,
        lapNo: lapNo,
        startDatasetFileNo: startDatasetFileNo,
        endDatasetFileNo: endDatasetFileNo,
        fullLap: fullLap,
        duration: duration,
        durationFormatted: durationFormatted,
        averages: averages
    };
    return new Lap(lap).save()
        .then(res => {
            console.log('Saved Lap (fileId:sessionId:lapNo): (' + res.fileId + ':' + res.sessionId + ':' + res.lapNo + ')');
            return res.id;
        })
        .catch((error) => {
            console.log('Error saving parsed Session', error);
            return;
        });
}

async function saveDataset(fileId, sessionId, lapId, datasetFileNo, values, headers) {
    const parameters = {};
    for (let i = 0; i < values.length; i++) {
        parameters[headers[i]] = values[i];
    }

    const dataset = {
        fileId: fileId,
        sessionId: sessionId,
        lapId: lapId,
        datasetFileNo: datasetFileNo,
        parameters: parameters
    };

    return new Dataset(dataset).save()
        .then(res => {
            return res.id;
        })
        .catch((error) => {
            console.log('Error saving parsed Session', error);
            return;
        });
}

async function parseFile(name, extension) {
    if (extension && extension !== '.csv') {
        console.log('File not supported.');
        return;
    }

    if (!parserRunning) {
        parserRunning = true;

        const dataFromCsv = await parseCsv(name);

        parserRunning = false;
        const index = parseQueue.indexOf(name);
        if (index > -1) {
            parseQueue.splice(index, 1);
        }

        const fileId = await File.findOne({
                name: name,
                extension: '.csv'
            })
            .exec()
            .then((doc) => {
                return doc.id;
            })
            .catch((err) => {
                console.log('Error finding File', err);
                return;
            });

        // TODO: call revert function when one save fails
        const sessionId = await saveSession(fileId, name, dataFromCsv.venue, dataFromCsv.vehicleId, dataFromCsv.user, dataFromCsv.date,
            dataFromCsv.time, dataFromCsv.duration, dataFromCsv.datasetCount, dataFromCsv.fullLaps);

        for (let i = 0; i < dataFromCsv.laps.length; i++) {
            const lapId = await saveLap(fileId, sessionId, name, dataFromCsv.laps[i].lapNo, dataFromCsv.laps[i].startDatasetFileNo,
                dataFromCsv.laps[i].endDatasetFileNo, dataFromCsv.laps[i].fullLap, dataFromCsv.laps[i].duration,
                dataFromCsv.laps[i].durationFormatted, dataFromCsv.laps[i].averages);

            for (let j = dataFromCsv.laps[i].startDatasetFileNo; j <= dataFromCsv.laps[i].endDatasetFileNo; j++) {
                const datasetId = await saveDataset(fileId, sessionId, lapId, j, dataFromCsv.values[j], dataFromCsv.headers);
            }
            console.log('Saved all Datasets for: (' + fileId + ':' + sessionId + ':' + dataFromCsv.laps[i].lapNo + ')');
        }

        await File.findOne({
                _id: fileId
            })
            .exec()
            .then((doc) => {
                doc.parsed = true;
                return doc.save()
                    .then(res => {
                        console.log(res.name + res.extension + ' is parsed + saved.');
                        return;
                    })
                    .catch((error) => {
                        console.log('Error updating parsed File', error);
                        return;
                    });
            })
            .catch((err) => {
                console.log('Error finding File', err);
                return;
            });

        if (parseQueue.length > 0) {
            parseFile(parseQueue[0]);
        }
    } else {
        parseQueue.push(name);
    }
}

async function parseCsv(name) {
    console.log('Parsing File: ', name + '.csv');
    let csvByLine = [];
    return await new Promise((resolve, reject) => {
        csv.fromPath('./server/sessions/' + name + '.csv')
            .on("data", (data) => {
                csvByLine.push(data);
            })
            .on("error", reject)
            .on("end", (count) => {
                let dataFromCsv = extractData(csvByLine);
                dataFromCsv.sessionName = name;
                console.log('> Finished parsing ' + name + '.csv with ' + count + ' lines.');
                return resolve(dataFromCsv);
            });
    });
}

function extractData(csvByLine) {
    const sessionName = '';
    const venue = csvByLine[1][1];
    const vehicleId = csvByLine[2][1];
    const user = csvByLine[3][1];
    const date = csvByLine[6][1];
    const time = csvByLine[7][1];
    const duration = utilCtrl.formatSecs(csvByLine[9][1]);
    const headers = csvByLine[14];

    csvByLine.splice(0, 19);
    const values = csvByLine;
    const datasetCount = values.length;

    let laps = [];
    let fullLaps = 0;

    /** Find lap starts by time + distance*/
    for (let i = 0; i < values.length; i++) {
        if (values[i][0] === '0.000' && values[i][1] === '0.000') {
            laps.push({
                'lapNo': laps.length + 1,
                'startDatasetFileNo': i
            });
        }
    }

    /** Find end for each lap */
    for (let i = 0; i < laps.length; i++) {
        if (i + 1 < laps.length) {
            laps[i].endDatasetFileNo = laps[i + 1].startDatasetFileNo - 1;
        } else {
            laps[i].endDatasetFileNo = values.length - 1;
        }
        if (Number(values[laps[i].endDatasetFileNo][1]) > 24) {
            laps[i].fullLap = true;
            fullLaps++;
        } else {
            laps[i].fullLap = false;
        }
    }

    /** Add lap data */
    for (let i = 0; i < laps.length; i++) {
        laps[i].datasets = laps[i].endDatasetFileNo - laps[i].startDatasetFileNo + 1;
        laps[i].duration = values[laps[i].endDatasetFileNo][0];
        laps[i].durationFormatted = utilCtrl.formatSecs(values[laps[i].endDatasetFileNo][0]);

        laps[i].averages = {};
        for (let k = 0; k < headers.length; k++) {
            let count = 0;
            for (let j = laps[i].startDatasetFileNo; j <= laps[i].endDatasetFileNo; j++) {
                count = count + Number(values[j][k]);
            }
            laps[i].averages[headers[k]] = count / laps[i].datasets;
        }
    }

    return {
        sessionName,
        venue,
        vehicleId,
        user,
        date,
        time,
        duration,
        datasetCount,
        fullLaps,
        laps,
        headers,
        values
    };
}

module.exports = {
    parseFile
}
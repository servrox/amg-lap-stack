function formatSecs(secString) {
    if (!secString) {
        return '00:00:00';
    }
    let milliseconds = 0;
    if (secString.split('.').length > 1) {
        milliseconds = secString.split('.')[1];
    }
    let secs = parseInt(secString, 10);
    let hours = Math.floor(secs / 3600);
    let minutes = Math.floor((secs - (hours * 3600)) / 60);
    let seconds = secs - (hours * 3600) - (minutes * 60);

    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return minutes + ':' + seconds + ':' + milliseconds;
}

module.exports = {
    formatSecs
}